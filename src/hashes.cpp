#include <boxes/hashes.hpp>

namespace boxes::hash {

XXHash64::XXHash64(uint64_t seed)
    : seed{seed}, state{nullptr, XXH64_freeState} {
  state.reset(XXH64_createState());
}

void XXHash64::reset() { XXH64_reset(state.get(), seed); }

void XXHash64::operator()(const uint8_t *key, std::size_t len) {
  if (XXH_ERROR == XXH64_update(state.get(), key, len)) {
    throw std::runtime_error("XXH64_update failed");
  }
}

uint64_t XXHash64::final() const { return XXH64_digest(state.get()); }

#define BIG_CONSTANT(x) (x##LLU)

Murmur2A_X64_64::Murmur2A_X64_64(uint64_t seed) : seed{seed} {}

void Murmur2A_X64_64::reset() { h = seed; }

void Murmur2A_X64_64::operator()(const uint8_t *key, std::size_t len) {
  const uint64_t m = BIG_CONSTANT(0xc6a4a7935bd1e995);
  const int r = 47;

  h ^= (len * m);

  const uint64_t *data = (const uint64_t *)key;
  const uint64_t *end = data + (len / 8);

  while (data != end) {
    uint64_t k = *data++;

    k *= m;
    k ^= k >> r;
    k *= m;

    h ^= k;
    h *= m;
  }

  const unsigned char *data2 = (const unsigned char *)data;

  switch (len & 7) {
  case 7:
    h ^= uint64_t(data2[6]) << 48;
    [[fallthrough]];
  case 6:
    h ^= uint64_t(data2[5]) << 40;
    [[fallthrough]];
  case 5:
    h ^= uint64_t(data2[4]) << 32;
    [[fallthrough]];
  case 4:
    h ^= uint64_t(data2[3]) << 24;
    [[fallthrough]];
  case 3:
    h ^= uint64_t(data2[2]) << 16;
    [[fallthrough]];
  case 2:
    h ^= uint64_t(data2[1]) << 8;
    [[fallthrough]];
  case 1:
    h ^= uint64_t(data2[0]);
    h *= m;
  };

  h ^= h >> r;
  h *= m;
  h ^= h >> r;
}

uint64_t Murmur2A_X64_64::final() const { return h; }

BJOneAtATime::BJOneAtATime(uint64_t seed)
    : seed{static_cast<uint32_t>(seed & 0xffffffff)} {}

void BJOneAtATime::reset() { hash = seed; }

void BJOneAtATime::operator()(const uint8_t *key, std::size_t length) {
  size_t i = 0;
  while (i != length) {
    hash += key[i++];
    hash += hash << 10;
    hash ^= hash >> 6;
  }
  hash += hash << 3;
  hash ^= hash >> 11;
  hash += hash << 15;
}

uint64_t BJOneAtATime::final() const { return hash; }

} // namespace boxes::hash
