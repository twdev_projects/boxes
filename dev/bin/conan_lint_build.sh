#!/bin/bash

declare -r SELF=$(readlink -f "${BASH_SOURCE[0]}")
declare -r SELFDIR=$(dirname "${SELF}")
declare -r PROJECT_ROOT="${SELFDIR}/../.."

declare -r IMAGENAME="conanlint"
declare -r VERSION="1.0-bookworm"

docker build \
    -f "${PROJECT_ROOT}/docker/${IMAGENAME}/Dockerfile" \
    -t "${IMAGENAME}:${VERSION}" \
    -t "${IMAGENAME}:latest" \
    "${PROJECT_ROOT}/docker/${IMAGENAME}"
