#!/bin/bash

declare -r SELF=$(readlink -f "${BASH_SOURCE[0]}")
declare -r SELFDIR=$(dirname "${SELF}")
declare -r PROJECT_ROOT="${SELFDIR}/../.."

declare -r IMAGENAME="conanlint"
declare -r VERSION="1.0-bookworm"

docker run \
    --rm \
    -v "${PROJECT_ROOT}:/usr/src" \
    -it "${IMAGENAME}:${VERSION}" \
    "$@"
