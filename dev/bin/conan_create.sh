#!/bin/bash

declare -r SELF=$(readlink -f "${BASH_SOURCE[0]}")
declare -r SELFDIR=$(dirname "${SELF}")
declare -r PROJECT_ROOT="${SELFDIR}/../.."

VARIANT="${1:-dev}"
VERSION="${2:-0.1.1}"
PREFIX="${PROJECT_ROOT}/conan/${VARIANT}/all"

conan create \
    "$PREFIX/conanfile.py" \
    -tf "$PREFIX/test_package" \
    --settings=compiler.cppstd=20 \
    --build=missing \
    --version="$VERSION" 
