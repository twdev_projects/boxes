#include <benchmark/benchmark.h>

#include <boxes/bloom_filter.hpp>

#include <random>

using namespace boxes;

static void BM_BloomKMMurmurAndMurmur(benchmark::State &state) {
  using namespace boxes::filter::bloom;
  using namespace boxes::hash;

  const std::size_t maxElements = 10000;
  const std::size_t maxStored = 1000;
  auto filter = makeWithKirschMitzenmacherFamily<int>(
      maxElements, 0.001, makeHashWithRandomSeed<Murmur2A_X64_64>(),
      makeHashWithRandomSeed<Murmur2A_X64_64>());

  for (std::size_t i = 0; i < maxStored; ++i) {
    filter.insert(i);
  }

  for (auto _ : state) {
    for (std::size_t i = 0; i < maxStored; ++i) {
      benchmark::DoNotOptimize(filter.contains(i));
    }
  }
}

static void BM_BloomKMBJOneAtATimeAndBJOneAtATime(benchmark::State &state) {
  using namespace boxes::filter::bloom;
  using namespace boxes::hash;

  const std::size_t maxElements = 10000;
  const std::size_t maxStored = 1000;
  auto filter = makeWithKirschMitzenmacherFamily<int>(
      maxElements, 0.001, makeHashWithRandomSeed<BJOneAtATime>(),
      makeHashWithRandomSeed<BJOneAtATime>());

  for (std::size_t i = 0; i < maxStored; ++i) {
    filter.insert(i);
  }

  for (auto _ : state) {
    for (std::size_t i = 0; i < maxStored; ++i) {
      benchmark::DoNotOptimize(filter.contains(i));
    }
  }
}

static void BM_BloomKMBJOneAtATimeAndXXHash64(benchmark::State &state) {
  using namespace boxes::filter::bloom;
  using namespace boxes::hash;

  const std::size_t maxElements = 10000;
  const std::size_t maxStored = 1000;
  auto filter = makeWithKirschMitzenmacherFamily<int>(
      maxElements, 0.001, makeHashWithRandomSeed<BJOneAtATime>(),
      makeHashWithRandomSeed<XXHash64>());

  for (std::size_t i = 0; i < maxStored; ++i) {
    filter.insert(i);
  }

  for (auto _ : state) {
    for (std::size_t i = 0; i < maxStored; ++i) {
      benchmark::DoNotOptimize(filter.contains(i));
    }
  }
}

static void BM_BloomKMXXHash64AndXXHash64(benchmark::State &state) {
  using namespace boxes::filter::bloom;
  using namespace boxes::hash;

  const std::size_t maxElements = 10000;
  const std::size_t maxStored = 1000;
  auto filter = makeWithKirschMitzenmacherFamily<int>(
      maxElements, 0.001, makeHashWithRandomSeed<XXHash64>(),
      makeHashWithRandomSeed<XXHash64>());

  for (std::size_t i = 0; i < maxStored; ++i) {
    filter.insert(i);
  }

  for (auto _ : state) {
    for (std::size_t i = 0; i < maxStored; ++i) {
      benchmark::DoNotOptimize(filter.contains(i));
    }
  }
}

static void BM_BloomHashFamilyWithXXHash(benchmark::State &state) {
  using namespace boxes::filter::bloom;
  using namespace boxes::hash;

  const std::size_t maxElements = 10000;
  const std::size_t maxStored = 1000;
  auto filter = makeWithHashWithSeedFamily<int, XXHash64>(maxElements, 0.001);

  for (std::size_t i = 0; i < maxStored; ++i) {
    filter.insert(i);
  }

  for (auto _ : state) {
    for (std::size_t i = 0; i < maxStored; ++i) {
      benchmark::DoNotOptimize(filter.contains(i));
    }
  }
}

static void BM_BloomHashFamilyWithBJOneAtATime(benchmark::State &state) {
  using namespace boxes::filter::bloom;
  using namespace boxes::hash;

  const std::size_t maxElements = 10000;
  const std::size_t maxStored = 1000;
  auto filter =
      makeWithHashWithSeedFamily<int, BJOneAtATime>(maxElements, 0.001);

  for (std::size_t i = 0; i < maxStored; ++i) {
    filter.insert(i);
  }

  for (auto _ : state) {
    for (std::size_t i = 0; i < maxStored; ++i) {
      benchmark::DoNotOptimize(filter.contains(i));
    }
  }
}

static void BM_BloomHashFamilyWithMurmur(benchmark::State &state) {
  using namespace boxes::filter::bloom;
  using namespace boxes::hash;

  const std::size_t maxElements = 10000;
  const std::size_t maxStored = 1000;
  auto filter =
      makeWithHashWithSeedFamily<int, Murmur2A_X64_64>(maxElements, 0.001);

  for (std::size_t i = 0; i < maxStored; ++i) {
    filter.insert(i);
  }

  for (auto _ : state) {
    for (std::size_t i = 0; i < maxStored; ++i) {
      benchmark::DoNotOptimize(filter.contains(i));
    }
  }
}

BENCHMARK(BM_BloomKMMurmurAndMurmur);
BENCHMARK(BM_BloomKMBJOneAtATimeAndBJOneAtATime);
BENCHMARK(BM_BloomKMBJOneAtATimeAndXXHash64);
BENCHMARK(BM_BloomKMXXHash64AndXXHash64);
BENCHMARK(BM_BloomHashFamilyWithXXHash);
BENCHMARK(BM_BloomHashFamilyWithBJOneAtATime);
BENCHMARK(BM_BloomHashFamilyWithMurmur);

// Run the benchmark
BENCHMARK_MAIN();
