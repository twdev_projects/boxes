#include <gtest/gtest.h>

#include <boxes/ring_buffer.hpp>

using namespace boxes;

class RingBufferTest : public ::testing::Test {
public:
  void SetUp() override {}
  void TearDown() override {}

protected:
};

TEST_F(RingBufferTest, test_PushPopOnTrivialType) {
  RingBuffer<int, 4> ring_buffer{};
  auto value = 1234;
  ring_buffer.push_back(value);
  ASSERT_EQ(ring_buffer.front(), value);
  ASSERT_EQ(ring_buffer.back(), value);
  ASSERT_EQ(ring_buffer.size(), 1);

  ring_buffer.pop_back();
  ASSERT_EQ(ring_buffer.size(), 0);
}

TEST_F(RingBufferTest, test_PushPopDataInspection) {
  RingBuffer<int, 16> ring_buffer{};
  const int v[] = {1, 2, 3, 4};
  for (auto i : v) {
    ring_buffer.push_back(i);
  }

  ASSERT_EQ(ring_buffer.front(), v[0]);
  ASSERT_EQ(ring_buffer.back(), v[3]);

  ring_buffer.pop_back();
  ASSERT_EQ(ring_buffer.front(), v[0]);
  ASSERT_EQ(ring_buffer.back(), v[2]);

  ring_buffer.pop_front();
  ASSERT_EQ(ring_buffer.front(), v[1]);
  ASSERT_EQ(ring_buffer.back(), v[2]);
}

TEST_F(RingBufferTest, test_PushFront) {
  RingBuffer<int, 16> ring_buffer{};
  const int v[] = {1, 2, 3, 4};
  for (auto i : v) {
    ring_buffer.push_front(i);
  }

  ASSERT_EQ(ring_buffer.front(), v[3]);
  ASSERT_EQ(ring_buffer.back(), v[0]);

  ring_buffer.pop_back();
  ASSERT_EQ(ring_buffer.front(), v[3]);
  ASSERT_EQ(ring_buffer.back(), v[1]);

  ring_buffer.pop_front();
  ASSERT_EQ(ring_buffer.front(), v[2]);
  ASSERT_EQ(ring_buffer.back(), v[1]);
}

TEST_F(RingBufferTest, test_OperationsSpanningRingSize) {
  RingBuffer<int, 3> r{};
  const int v[] = {1, 2, 3, 4, 5, 6, 7};

  ASSERT_TRUE(r.push_back(v[0]));
  ASSERT_TRUE(r.push_back(v[1]));
  ASSERT_TRUE(r.push_back(v[2]));

  ASSERT_FALSE(r.push_back(v[3]));
  ASSERT_TRUE(r.full());
  ASSERT_EQ(r.size(), 3);
  ASSERT_EQ(r.front(), v[0]);
  ASSERT_EQ(r.back(), v[2]);

  r.pop_front();
  ASSERT_TRUE(r.push_back(v[3]));
  ASSERT_EQ(r.front(), v[1]);
  ASSERT_EQ(r.back(), v[3]);

  r.pop_front();
  r.pop_back();
  ASSERT_TRUE(r.push_back(v[4]));
  ASSERT_TRUE(r.push_back(v[5]));
  ASSERT_EQ(r.front(), v[2]);
  ASSERT_EQ(r.back(), v[5]);

  r.pop_back();
  ASSERT_TRUE(r.push_front(v[6]));
  ASSERT_EQ(r.front(), v[6]);
  ASSERT_EQ(r.back(), v[4]);
  ASSERT_TRUE(r.full());
}

namespace {
class X {
public:
  X() { destroyed = false; }
  ~X() { destroyed = true; }
  static bool destroyed;
};
bool X::destroyed = false;
} // namespace

TEST_F(RingBufferTest, test_DestructionOnPopBack) {
  RingBuffer<std::unique_ptr<X>, 3> r{};
  r.push_back(std::make_unique<X>());
  r.pop_back();
  ASSERT_TRUE(r.empty());
  ASSERT_TRUE(X::destroyed);
}

TEST_F(RingBufferTest, test_DestructionOnPopFront) {
  RingBuffer<std::unique_ptr<X>, 3> r{};
  r.push_back(std::make_unique<X>());
  r.pop_front();
  ASSERT_TRUE(r.empty());
  ASSERT_TRUE(X::destroyed);
}

TEST_F(RingBufferTest, test_DestructionOnClear) {
  RingBuffer<std::unique_ptr<X>, 3> r{};
  r.push_back(std::make_unique<X>());
  r.clear();
  ASSERT_TRUE(r.empty());
  ASSERT_TRUE(X::destroyed);
}

TEST_F(RingBufferTest, test_ForwardIteration) {
  RingBuffer<int, 4> r{};
  const int v[] = {1, 2, 3, 4};
  for (auto i : v) {
    r.push_back(i);
  }

  int i = 0;
  for (auto it = r.begin(); it != r.end(); ++it) {
    ASSERT_EQ(*it, v[i++]);
  }

  // C++11 style for loop
  i = 0;
  for (const auto &x : r) {
    ASSERT_EQ(x, v[i++]);
  }
}

TEST_F(RingBufferTest, test_ifComparisonWorks) {
  RingBuffer<int, 4> r1{};
  RingBuffer<int, 4> r2{};

  ASSERT_TRUE(r1 == r2);
  ASSERT_FALSE(r1 != r2);

  r1.push_back(1);
  ASSERT_FALSE(r1 == r2);
  ASSERT_TRUE(r1 != r2);

  r2.push_back(1);
  ASSERT_TRUE(r1 == r2);
  ASSERT_FALSE(r1 != r2);

  r1.push_back(2);
  r2.push_back(3);
  ASSERT_FALSE(r1 == r2);
  ASSERT_TRUE(r1 != r2);
}

TEST_F(RingBufferTest, test_ifSwapWorks) {
  RingBuffer<int, 4> r1{};
  RingBuffer<int, 4> r2{};

  r1.push_back(1);
  r1.push_back(2);
  r2.push_back(3);
  r2.push_back(4);

  r1.swap(r2);
  ASSERT_EQ(r1.size(), 2);
  ASSERT_EQ(r2.size(), 2);
  ASSERT_EQ(r1.front(), 3);
  ASSERT_EQ(r1.back(), 4);
  ASSERT_EQ(r2.front(), 1);
  ASSERT_EQ(r2.back(), 2);
}

TEST_F(RingBufferTest, test_ifMoveWorks) {
  RingBuffer<int, 4> r1{};
  r1.push_back(1);
  r1.push_back(2);
  r1.push_back(3);
  r1.push_back(4);

  RingBuffer<int, 4> r2{std::move(r1)};
  ASSERT_EQ(r2.size(), 4);
  ASSERT_EQ(r2.front(), 1);
  ASSERT_EQ(r2.back(), 4);
  ASSERT_EQ(r1.size(), 0);
}

TEST_F(RingBufferTest, test_ifMoveAssignmentWorks) {
  RingBuffer<int, 4> r1{};
  r1.push_back(1);
  r1.push_back(2);
  r1.push_back(3);
  r1.push_back(4);

  RingBuffer<int, 4> r2{};
  r2.push_back(5);
  r2.push_back(6);
  r2.push_back(7);
  r2.push_back(8);

  r2 = std::move(r1);
  ASSERT_EQ(r2.size(), 4);
  ASSERT_EQ(r2.front(), 1);
  ASSERT_EQ(r2.back(), 4);
  ASSERT_EQ(r1.size(), 0);
}

TEST_F(RingBufferTest, test_ifCopyWorks) {
  RingBuffer<int, 4> r1{};
  r1.push_back(1);
  r1.push_back(2);
  r1.push_back(3);
  r1.push_back(4);

  RingBuffer<int, 4> r2{r1};
  ASSERT_EQ(r2.size(), 4);
  ASSERT_EQ(r2.front(), 1);
  ASSERT_EQ(r2.back(), 4);
  ASSERT_EQ(r1.size(), 4);
  ASSERT_EQ(r1.front(), 1);
  ASSERT_EQ(r1.back(), 4);
}

TEST_F(RingBufferTest, test_ifCopyAssignmentWorks) {
  RingBuffer<int, 4> r1{};
  r1.push_back(1);
  r1.push_back(2);
  r1.push_back(3);
  r1.push_back(4);

  RingBuffer<int, 4> r2{};
  r2.push_back(5);
  r2.push_back(6);
  r2.push_back(7);
  r2.push_back(8);

  r2 = r1;
  ASSERT_EQ(r2.size(), 4);
  ASSERT_EQ(r2.front(), 1);
  ASSERT_EQ(r2.back(), 4);
  ASSERT_EQ(r1.size(), 4);
  ASSERT_EQ(r1.front(), 1);
  ASSERT_EQ(r1.back(), 4);
}

TEST_F(RingBufferTest, test_ifMoveAssignmentWorksOnSelf) {
  RingBuffer<int, 4> r1{};
  r1.push_back(1);
  r1.push_back(2);
  r1.push_back(3);
  r1.push_back(4);

  r1 = std::move(r1);
  ASSERT_EQ(r1.size(), 4);
  ASSERT_EQ(r1.front(), 1);
  ASSERT_EQ(r1.back(), 4);
}
