#include <gtest/gtest.h>

#include <boxes/util.hpp>

#include <mutex>
#include <thread>

class UtilTest : public ::testing::Test {
public:
protected:
};

TEST_F(UtilTest, test_ifWithoutLockUnlocksTheGivenLock) {
  std::timed_mutex m;
  std::unique_lock<std::timed_mutex> l(m);
  boxes::util::withoutLock(l, [&]() {
    const auto isAcquired = l.try_lock_for(std::chrono::milliseconds(100));
    ASSERT_TRUE(isAcquired);
    l.unlock();
  });

  // check if locked back again
  ASSERT_THROW(
      { ASSERT_FALSE(l.try_lock_for(std::chrono::milliseconds(100))); },
      std::system_error);
}

TEST_F(UtilTest, test_ifUniqueAccessAllowsForDataAccess) {
  auto u = boxes::util::withUniqueAccess<std::mutex>(123);
  *u = 456;
  ASSERT_EQ(*u, 456);
}
