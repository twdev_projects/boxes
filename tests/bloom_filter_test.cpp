#include <gtest/gtest.h>

#include <boxes/bloom_filter.hpp>

#include <cmath>
#include <functional>

using namespace boxes::filter::bloom;
using namespace boxes::hash;

class BloomFilterTest : public ::testing::Test {};

class BloomFilterFalsePositiveRateTest : public ::testing::Test {
public:
protected:
  std::size_t expectedMaxElements;
  double expectedErrorRate;

  double calculateP(std::size_t m, std::size_t n, std::size_t k) {
    return std::pow(1 - std::exp(-1 * static_cast<double>(k) /
                                 (static_cast<double>(m) / n)),
                    k);
  }

  template <typename FilterT> void testFalsePositiveRate(FilterT &filter) {
    ASSERT_GT(filter.k(), 1);
    ASSERT_GT(filter.capacity(), this->expectedMaxElements);
    ASSERT_EQ(filter.count(), 0);

    for (std::size_t i = 0; i < this->expectedMaxElements; i++) {
      filter.insert(i);
    }

    int falsePositives = 0;
    for (std::size_t i = 0; i < this->expectedMaxElements; i++) {
      const auto v = i + this->expectedMaxElements;
      if (filter.contains(v)) {
        falsePositives++;
      }
    }

    ASSERT_GT(filter.count(), this->expectedMaxElements / 2);

    const auto theoreticalP =
        calculateP(filter.capacity(), this->expectedMaxElements, filter.k());

    const auto errorRate = static_cast<double>(falsePositives) /
                           static_cast<double>(this->expectedMaxElements);

    auto acceptanceFactor = 1.2;
    ASSERT_LT(errorRate, acceptanceFactor * theoreticalP);
  }
};

template <Hash64 HT>
class BloomFilterHashWithSeedFamilyTest
    : public BloomFilterFalsePositiveRateTest {
public:
  void SetUp() override {
    expectedMaxElements = 1000000;
    expectedErrorRate = 0.01;
    filter = std::make_unique<Bloom<int, HashWithSeedFamily<HT>>>(
        makeWithHashWithSeedFamily<int, HT>(expectedMaxElements,
                                            expectedErrorRate));
  }

  void TearDown() override { filter.reset(); }

protected:
  std::unique_ptr<Bloom<int, HashWithSeedFamily<HT>>> filter;
};

template <typename HashPairT>
class BloomFilterKirschMitzenmacherFamilyTest
    : public BloomFilterFalsePositiveRateTest {
public:
  using H1T = typename HashPairT::first_type;
  using H2T = typename HashPairT::second_type;

  void SetUp() override {
    expectedMaxElements = 1000000;
    expectedErrorRate = 0.01;

    auto h1 = makeHashWithRandomSeed<H1T>();
    auto h2 = makeHashWithRandomSeed<H2T>();

    filter = std::make_unique<Bloom<int, KirschMitzenmacher<H1T, H2T>>>(
        makeWithKirschMitzenmacherFamily<int, H1T, H2T>(
            expectedMaxElements, expectedErrorRate, std::move(h1),
            std::move(h2)));
  }

  void TearDown() override { filter.reset(); }

protected:
  std::unique_ptr<Bloom<int, KirschMitzenmacher<H1T, H2T>>> filter;
};

TEST_F(BloomFilterTest, test_ifReturnsFalseWhenEmpty) {
  auto filter = makeWithHashWithSeedFamily<int, XXHash64>(100, 0.01);
  ASSERT_FALSE(filter.contains(1));
  ASSERT_FALSE(filter.contains(2));
  ASSERT_FALSE(filter.contains(3));
  ASSERT_FALSE(filter.contains(4));
  ASSERT_FALSE(filter.contains(5));
}

TEST_F(BloomFilterTest, test_ifReturnsTrueWhenContains) {
  auto filter = makeWithHashWithSeedFamily<int, XXHash64>(100, 0.01);
  filter.insert(1);
  filter.insert(2);
  filter.insert(3);
  filter.insert(4);
  filter.insert(5);
  ASSERT_TRUE(filter.contains(1));
  ASSERT_TRUE(filter.contains(2));
  ASSERT_TRUE(filter.contains(3));
  ASSERT_TRUE(filter.contains(4));
  ASSERT_TRUE(filter.contains(5));
}

TEST_F(BloomFilterTest, test_ifReturnsFalseWhenNotContains) {
  auto filter = makeWithHashWithSeedFamily<int, XXHash64>(100, 0.01);
  filter.insert(1);
  filter.insert(2);
  filter.insert(3);
  filter.insert(4);
  filter.insert(5);
  ASSERT_FALSE(filter.contains(6));
  ASSERT_FALSE(filter.contains(7));
  ASSERT_FALSE(filter.contains(8));
  ASSERT_FALSE(filter.contains(9));
  ASSERT_FALSE(filter.contains(10));
}

using Hashes = ::testing::Types<XXHash64, Murmur2A_X64_64, BJOneAtATime>;

using HashPairs = ::testing::Types<
    std::pair<XXHash64, XXHash64>, std::pair<Murmur2A_X64_64, Murmur2A_X64_64>,
    std::pair<BJOneAtATime, BJOneAtATime>, std::pair<XXHash64, Murmur2A_X64_64>,
    std::pair<XXHash64, BJOneAtATime>, std::pair<Murmur2A_X64_64, XXHash64>,
    std::pair<Murmur2A_X64_64, BJOneAtATime>, std::pair<BJOneAtATime, XXHash64>,
    std::pair<BJOneAtATime, Murmur2A_X64_64>>;

TYPED_TEST_SUITE(BloomFilterHashWithSeedFamilyTest, Hashes);

TYPED_TEST_SUITE(BloomFilterKirschMitzenmacherFamilyTest, HashPairs);

TYPED_TEST(BloomFilterHashWithSeedFamilyTest,
           test_ifEstimatedErrorRateIsBelowCalculated) {
  this->testFalsePositiveRate(*this->filter);
}

TYPED_TEST(BloomFilterKirschMitzenmacherFamilyTest,
           test_ifEstimatedErrorRateIsBelowCalculated) {
  this->testFalsePositiveRate(*this->filter);
}
