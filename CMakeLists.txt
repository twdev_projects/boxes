cmake_minimum_required(VERSION 3.8)
project(libboxes LANGUAGES CXX)

include(GNUInstallDirs)
include(CMakePackageConfigHelpers)

find_package(xxHash 0.8.2 REQUIRED CONFIG)

find_library(M_LIB m)
if (M_LIB)
    set (M_LIB m)
else()
    set (M_LIB "")
endif()

option(BUILD_EXAMPLES "Build examples" ON)
option(BUILD_BENCHMARKS "Build benchmarks" ON)

set(LIBBOXES_HEADERS
    include/boxes/bloom_filter.hpp
    include/boxes/boxes.hpp
    include/boxes/cache.hpp
    include/boxes/compiler.hpp
    include/boxes/hash_utils.hpp
    include/boxes/hashes.hpp
    include/boxes/linked_vector.hpp
    include/boxes/ring_buffer.hpp
)

add_library(libboxes src/hashes.cpp ${LIBBOXES_HEADERS})
target_include_directories(libboxes PUBLIC
    $<BUILD_INTERFACE:${CMAKE_CURRENT_SOURCE_DIR}/include>
    $<INSTALL_INTERFACE:include>)
target_compile_features(libboxes PUBLIC cxx_std_20)

target_link_libraries(libboxes
    PUBLIC
    ${M_LIB}
    xxHash::xxhash
)

set_target_properties(libboxes PROPERTIES
    PUBLIC_HEADER "${LIBBOXES_HEADERS}"
    OUTPUT_NAME boxes
)

install(TARGETS libboxes
    EXPORT libboxes-targets
    LIBRARY DESTINATION ${CMAKE_INSTALL_LIBDIR}
    ARCHIVE DESTINATION ${CMAKE_INSTALL_LIBDIR}
    RUNTIME DESTINATION ${CMAKE_INSTALL_BINDIR}
    PUBLIC_HEADER DESTINATION ${CMAKE_INSTALL_INCLUDEDIR}/boxes
)

install(EXPORT libboxes-targets
    FILE libboxesTargets.cmake
    NAMESPACE libboxes::
    DESTINATION ${CMAKE_INSTALL_LIBDIR}/cmake/libboxes
)

if (BUILD_EXAMPLES)
    add_subdirectory(examples)
endif()

if (BUILD_TESTING)
    add_subdirectory(tests)
    if (BUILD_BENCHMARKS)
        add_subdirectory(benchmarks)
    endif()
endif()
