#ifndef __BOXES_UTIL_HPP__
#define __BOXES_UTIL_HPP__

#include <boxes/compiler.hpp>

namespace boxes::util {

template <typename LockableT, typename FuncT>
void withoutLock(LockableT &lockable, FuncT f) {
  lockable.unlock();
  f();
  lockable.lock();
}

template <typename LockableT, typename T> class UniqueAccess {
  class Guard;

public:
  template <typename... Args>
  UniqueAccess(Args &&...args) : data{std::forward<Args>(args)...} {}

  Guard operator*() { return Guard(*this); }

  Guard operator*() const { return Guard(*this); }

private:
  LockableT lockable;
  T data;

  class Guard {
  public:
    Guard(UniqueAccess &uniqueAccess)
        : uniqueAccess(uniqueAccess), lock(uniqueAccess.lockable) {}

    operator T &() { return uniqueAccess.data; }

    operator const T &() const { return uniqueAccess.data; }

    Guard &operator=(const T &data) {
      uniqueAccess.data = data;
      return *this;
    }

    bool operator==(const T &data) const BOXES_NOTHROW {
      return uniqueAccess.data == data;
    }

  private:
    UniqueAccess &uniqueAccess;
    std::unique_lock<LockableT> lock;
  };
};

template <typename LockableT, typename T> auto withUniqueAccess(T &&data) {
  return UniqueAccess<LockableT, T>(std::forward<T>(data));
}

} // namespace boxes::util

#endif // __BOXES_UTIL_HPP__
